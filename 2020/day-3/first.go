package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
)

const TreeChar = '#'

type mapLine struct {
	Original string
	Current  string
}

func (line *mapLine) getX(x int) rune {
	// Since the trees keep extending right we just add more
	// of the original when we need it
	if x >= len(line.Current) {
		line.Current += line.Original
		return line.getX(x)
	}

	return rune(line.Current[x])
}

type treeMap struct {
	Lines []*mapLine
}

func (mp *treeMap) getY(y int) (*mapLine, error) {
	// This error should pop up when the end of the
	// level is reached
	if y >= len(mp.Lines) {
		return nil, fmt.Errorf("out of bounds")
	}

	return mp.Lines[y], nil
}

func (mp *treeMap) TestSlope(x int, y int) int {
	currX, currY, trees := 0, 0, 0

	for {
		l, err := mp.getY(currY)
		if err != nil {
			break
		}

		char := l.getX(currX)
		if char == TreeChar {
			trees += 1
		}

		currX += x
		currY += y
	}

	return trees
}

func main() {
	input := helpers.MustInputStringArray("2020/day-3/input.txt")

	theMap := treeMap{}
	for _, line := range input {
		theMap.Lines = append(theMap.Lines, &mapLine{
			Original: line,
			Current:  line,
		})
	}

	fmt.Printf("First: encountered %d trees\n\n", theMap.TestSlope(3, 1))

	slopes := []struct {
		x int
		y int
	}{{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}}

	multiplied := 1
	for _, slope := range slopes {
		trees := theMap.TestSlope(slope.x, slope.y)
		multiplied *= trees
		fmt.Printf("Second: slope %d Right, down %d encountered %d trees\n", slope.x, slope.y, trees)
	}

	fmt.Printf("Multiplied together: %d\n", multiplied)
}
