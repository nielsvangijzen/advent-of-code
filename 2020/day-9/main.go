package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"sort"
)

func main() {
	input := helpers.MustInputIntArray("2020/day-9/input.txt")
	first(input, 25)
	second(input, 133015568)
}

func first(input []int, preambleLen int) {
	l := len(input)
	for i := preambleLen; i < l; i++ {
		if i >= l {
			return
		}

		if !CheckPreabmle(input[i-preambleLen:i], input[i], preambleLen) {
			fmt.Printf("First: first number without the property: %d\n", input[i])
			return
		}
	}
}

func CheckPreabmle(preamble []int, checkAgainst int, preambleLen int) bool {
	for i := 0; i < preambleLen; i++ {
		for j := 0; j < preambleLen; j++ {
			if i == j || preamble[i] == preamble[j] {
				continue
			}
			if preamble[i]+preamble[j] == checkAgainst {
				return true
			}
		}
	}

	return false
}

func second(input []int, target int) {
	for i := 0; i < len(input); i++ {
		current := 0
		index := i
		var nums []int

		for current < target {
			current += input[index]
			nums = append(nums, input[index])
			index++

			if current == target && len(nums) > 1 {
				sort.Ints(nums)
				fmt.Printf(
					"Second: %v add up to %d so the answer is %d + %d = %d",
					nums, target, nums[0], nums[len(nums)-1], nums[0]+nums[len(nums)-1],
				)
			}
		}
		nums = []int{}
	}
}
