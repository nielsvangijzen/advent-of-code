package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strconv"
	"strings"
)

func main() {
	input := helpers.MustInputStringArray("2020/day-8/input.txt")
	instructions := parseInstructions(input)

	cpu := NewCpu(instructions)
	first(cpu)
	second(input)
}

func first(cpu *CPU) {
	err := cpu.Run(true)

	if err != nil {
		fmt.Printf("First: accumulator has a value of: %d\n", cpu.Accumulator)
	}
}

func second(input []string) {
	_ = SecondReplace(input, "nop", "jmp")
	_ = SecondReplace(input, "jmp", "nop")
}

func SecondReplace(orig []string, old string, new string) error {
	for i := 0; i < len(orig); i++ {
		input := make([]string, len(orig))
		copy(input, orig)
		if strings.Index(input[i], old) >= 0 {
			input[i] = strings.Replace(input[i], old, new, -1)

			instructions := parseInstructions(input)

			cpu := NewCpu(instructions)
			if err := cpu.Run(true); err != nil {
				if cpu.IP == len(cpu.Code) {
					fmt.Println(err)
					fmt.Println(cpu)
					fmt.Printf("Second: accumulator has a value of: %d\n", cpu.Accumulator)
					//return nil
				}
			}
		}
	}

	return fmt.Errorf("no correct execution found")
}

func parseInstructions(input []string) (instructions []Executable) {
	for _, ins := range input {
		split := strings.Split(ins, " ")
		num, err := strconv.Atoi(split[1])
		if err != nil {
			fmt.Printf("Unable to parse %s\n", ins)
			continue
		}

		var parsed Executable

		switch split[0] {
		case "acc":
			parsed = Acc{num}
		case "jmp":
			parsed = Jmp{num}
		case "nop":
			parsed = Nop{num}
		default:
			fmt.Printf("Unables to parse instruction %s", split[0])
		}

		instructions = append(instructions, parsed)
	}

	return
}
