package main

import "fmt"

type CPU struct {
	Code []Executable

	IP             int
	Accumulator    int
	ExecutionCount int

	Visited []bool

	Running bool
}

func (cpu CPU) String() string {
	return fmt.Sprintf(
		"IP: %d, Accumulator: %d, ExecutionCount: %d",
		cpu.IP, cpu.Accumulator, cpu.ExecutionCount,
	)
}

func (cpu *CPU) execute() error {
	if cpu.ExecutionCount > 500 {
		return fmt.Errorf("execution count limit reached")
	}

	cpu.ExecutionCount += 1

	cpu.Code[cpu.IP].Execute(cpu)

	return nil
}

func (cpu *CPU) Run(checkVisit bool) error {
	cpu.Running = true
	for cpu.Running {

		if cpu.IP >= len(cpu.Code) {
			return fmt.Errorf("instruction pointer exceeds the code")
		}

		if checkVisit {
			if cpu.Visited[cpu.IP] {
				return fmt.Errorf("instruction at position %d already visited", cpu.IP)
			}

			cpu.Visited[cpu.IP] = true
		}

		err := cpu.execute()
		if err != nil {
			cpu.Running = false
			return err
		}
	}

	return nil
}

func NewCpu(instructions []Executable) *CPU {
	return &CPU{
		Code:           instructions,
		IP:             0,
		Accumulator:    0,
		ExecutionCount: 0,
		Visited:        make([]bool, len(instructions)),
	}
}

type Instruction struct {
	Value int
}

type Executable interface {
	Execute(cpu *CPU)
}

type Acc Instruction

func (a Acc) Execute(cpu *CPU) {
	cpu.Accumulator += a.Value
	cpu.IP += 1
}

type Jmp Instruction

func (j Jmp) Execute(cpu *CPU) {
	cpu.IP += j.Value
}

type Nop Instruction

func (_ Nop) Execute(cpu *CPU) {
	cpu.IP += 1
}
