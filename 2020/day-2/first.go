package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strconv"
	"strings"
)

type passwordRule struct {
	Raw       string
	Character string
	Min       int
	Max       int
	Password  string
}

func (rule passwordRule) Validate() bool {
	count := strings.Count(rule.Password, rule.Character)
	return count >= rule.Min && count <= rule.Max
}

func (rule passwordRule) GloriousTobogganCorporatePolicyValidation() bool {
	occurrences := 0
	if string(rule.Password[rule.Min-1]) == rule.Character {
		occurrences += 1
	}

	if len(rule.Password) >= rule.Max && string(rule.Password[rule.Max-1]) == rule.Character {
		occurrences += 1
	}

	return occurrences == 1
}

func main() {
	input := helpers.MustInputString("2020/day-2/input.txt")

	split := strings.Split(input, "\n")
	var valid []passwordRule
	var gloriousValid []passwordRule
	for _, rule := range split {
		parsed, err := parseRule(rule)
		if err != nil {
			fmt.Printf("error parsing %s: %s\n", rule, err)
			continue
		}

		if parsed.Validate() {
			valid = append(valid, parsed)
		}

		if parsed.GloriousTobogganCorporatePolicyValidation() {
			gloriousValid = append(gloriousValid, parsed)
		}
	}

	fmt.Printf("%d passwords are valid\n", len(valid))
	fmt.Printf("%d passwords are valid according to the glorious toboggan corporate password policy\n",
		len(gloriousValid))
}

func parseRule(row string) (rule passwordRule, err error) {
	rule.Raw = row

	parts := strings.Split(row, ": ")
	if len(parts) != 2 {
		err = fmt.Errorf("the whole line is malformed")
		return
	}

	rule.Password = parts[1]
	unparsedRule := strings.Split(parts[0], " ")
	if len(unparsedRule) != 2 {
		err = fmt.Errorf("password rule is malformed")
		return
	}

	if len(unparsedRule[1]) != 1 {
		err = fmt.Errorf("character is not 1 in length")
		return
	}

	rule.Character = unparsedRule[1]
	minMax := strings.Split(unparsedRule[0], "-")
	if len(minMax) != 2 {
		err = fmt.Errorf("quantifiers are malformed")
		return
	}

	minInt, err := strconv.ParseInt(minMax[0], 10, 32)
	if err != nil {
		return
	}

	maxInt, err := strconv.ParseInt(minMax[1], 10, 32)
	if err != nil {
		return
	}

	rule.Min = int(minInt)
	rule.Max = int(maxInt)
	return
}
