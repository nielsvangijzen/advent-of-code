package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strings"
)

func main() {
	input := helpers.MustInputString("2020/day-6/input.txt")
	groups := strings.Split(input, "\n\n")
	first(groups)
	second(groups)
}

func first(groups []string) {
	sum := 0
	for _, group := range groups {
		answered := map[int32]bool{}
		fullString := strings.Replace(group, "\n", "", -1)
		for _, char := range fullString {
			answered[char] = true
		}
		sum += len(answered)
	}

	fmt.Printf("First: Total sum: %d\n", sum)
}

func second(groups []string) {
	sum := 0
	for _, group := range groups {
		people := strings.Split(group, "\n")

		if len(people) == 1 {
			sum += len(people[0])
			continue
		}

		answered := map[int32]bool{}
		for _, char := range people[0] {
			answered[char] = true
		}

		for _, person := range people[1:] {
			if person == "" {
				continue
			}
			for char, _ := range answered {
				if strings.Index(person, string(char)) == -1 {
					answered[char] = false
				}
			}
		}

		for _, ans := range answered {
			if ans {
				sum += 1
			}
		}
	}

	fmt.Printf("Second: Total sum: %d\n", sum)
}
