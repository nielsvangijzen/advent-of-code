package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"math"
	"sort"
)

func main() {
	first(helpers.MustInputIntArray("2020/day-10/example_input.txt"))
}

func first(input []int) {
	sort.Ints(input)
	//highestJolt := input[len(input)-1]
	//deviceJolt := highestJolt + 3

	oneJoltDiff := 0
	threeJoltDiff := 0

	currentJolts := 0

	for {
		minJolts := currentJolts + 1
		maxJolts := currentJolts + 3
		var available []struct {
			index int
			jolts int
		}

		for index, adapter := range input {
			if adapter >= minJolts && adapter <= maxJolts {
				available = append(available, struct {
					index int
					jolts int
				}{index: index, jolts: adapter})
			} else {
				break
			}
		}

		if available == nil {
			fmt.Printf("OneDiff: %d, ThreeDiff: %d\n", oneJoltDiff, threeJoltDiff)
			return
		}

		diff := math.Abs(float64(currentJolts - available[0].jolts))
		if diff == 1 {
			oneJoltDiff++
		} else if diff == 3 {
			threeJoltDiff++
		}

		currentJolts = available[0].jolts
		input = append(input[0:available[0].index], input[available[0].index+1:]...)
	}
}
