# Day 4
I saw other people doing little readmes so here goes

# First
As I know the second part always throws in a little extra sauce, I decided to parse everything
into a neat struct. In Go unwrapping input into a struct is a little tedious, but it works!

Then after that I wrote a validate function for the Passport struct that just checked empty values
and if all of them were present a true was returned.

# Second
The second part was all about validation, I have a personal well-known vendetta against regex. So
that's out of the question. The only thing I could do is write custom validators for the strings.

I made a custom type for every property that needed more than a one liner, the hex hair color for
example became:
```go
type HexColor string
```

This means that `HexColor` is just a normal string, but I can attach extra `HexColor` specific
functions to it.

```go
type HexColor string

func (h HexColor) Validate() bool {
	if len(h) != 7 {
		return false
	}

	if h[0] != '#' {
		return false
	}

	rest := h[1:]

	for _, char := range rest {
		// If the single character is Atoi-able then it's valid anyhow
		if _, err := strconv.Atoi(string(char)); err == nil {
			continue
		} else {
			// Else we check if the string is within the allowed characters (screw regex)
			if helpers.StringInSlice(string(char), []string{"a", "b", "c", "d", "e", "f"}) {
				continue
			}
		}
		return false
	}

	return true
}
```
