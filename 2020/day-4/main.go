package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strconv"
	"strings"
)

type Passport struct {
	Byr int
	Iyr int
	Eyr int
	Hgt Height
	Hcl HexColor
	Ecl EyeColor
	Pid PassportID
	Cid string
}

type HexColor string
type EyeColor string
type PassportID string

type Height struct {
	Units string
	Value int
}

func (p Passport) ValidateFirst() bool {
	return p.Byr != 0 && p.Iyr != 0 && p.Eyr != 0 &&
		p.Hgt != (Height{}) && p.Hcl != "" && p.Ecl != "" && p.Pid != ""
}

func (p Passport) ValidateSecond() bool {
	if !(p.Byr >= 1920 && p.Byr <= 2002) {
		fmt.Printf("Birth year %d is invalid\n", p.Byr)
		return false
	}

	if !(p.Iyr >= 2010 && p.Iyr <= 2020) {
		fmt.Printf("Issue year %d is invalid\n", p.Iyr)
		return false
	}

	if !(p.Eyr >= 2020 && p.Eyr <= 2030) {
		fmt.Printf("Expiration year %d is invalid\n", p.Eyr)
		return false
	}

	if !p.Hgt.Validate() {
		fmt.Printf("Height %v is invalid\n", p.Hgt)
		return false
	}

	if !p.Ecl.Validate() {
		fmt.Printf("Eyecolor %s is invalid\n", p.Ecl)
		return false
	}

	if !p.Hcl.Validate() {
		fmt.Printf("Haircolor %s is invalid\n", p.Hcl)
		return false
	}

	if !p.Pid.Validate() {
		fmt.Printf("Passport ID %s is invalid\n", p.Pid)
		return false
	}

	return true
}

func (h HexColor) Validate() bool {
	if len(h) != 7 {
		return false
	}

	if h[0] != '#' {
		return false
	}

	rest := h[1:]

	for _, char := range rest {
		// If the single character is Atoi-able then it's valid anyhow
		if _, err := strconv.Atoi(string(char)); err == nil {
			continue
		} else {
			// Else we check if the string is within the allowed characters (screw regex)
			if helpers.StringInSlice(string(char), []string{"a", "b", "c", "d", "e", "f"}) {
				continue
			}
		}
		return false
	}

	return true
}

func (h EyeColor) Validate() bool {
	return helpers.StringInSlice(string(h), []string{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"})
}

func (h PassportID) Validate() bool {
	if len(h) != 9 {
		return false
	}

	for _, char := range h {
		if _, err := strconv.Atoi(string(char)); err != nil {
			return false
		}
	}

	return true
}

func (h Height) Validate() bool {
	switch h.Units {
	case "cm":
		return h.Value >= 150 && h.Value <= 193
	case "in":
		return h.Value >= 59 && h.Value <= 76
	}

	return false
}

func parsePassportString(pass string) Passport {
	sanitized := strings.Replace(pass, "\n", " ", -1)
	singleParts := strings.Split(sanitized, " ")
	passport := Passport{}

	for _, p := range singleParts {
		kv := strings.Split(p, ":")
		if len(kv) != 2 {
			fmt.Printf("Error processing %s", p)
			continue
		}
		key := kv[0]
		value := kv[1]

		switch key {
		case "byr":
			passport.Byr, _ = strconv.Atoi(value)
		case "iyr":
			passport.Iyr, _ = strconv.Atoi(value)
		case "eyr":
			passport.Eyr, _ = strconv.Atoi(value)
		case "hgt":
			units := value[len(value)-2:]
			digits := strings.Replace(value, units, "", 1)
			val, _ := strconv.Atoi(digits)
			passport.Hgt = Height{
				Units: units,
				Value: val,
			}
		case "hcl":
			passport.Hcl = HexColor(value)
		case "ecl":
			passport.Ecl = EyeColor(value)
		case "pid":
			passport.Pid = PassportID(value)
		case "cid":
			passport.Cid = value
		default:
			fmt.Printf("Key %s is unaccounted for\n", key)
		}
	}

	return passport
}

func main() {
	input := helpers.MustInputString("2020/day-4/input.txt")

	// Single passports are separated by a double linebreak
	passportString := strings.Split(input, "\n\n")

	valid := 0
	validSecond := 0
	for _, pass := range passportString {
		passport := parsePassportString(pass)
		if passport.ValidateFirst() {
			valid++
		}

		if passport.ValidateSecond() {
			validSecond++
		}
	}

	fmt.Printf("First: %d passports are valid\n", valid)
	fmt.Printf("Second: %d passports are valid\n", validSecond)
}
