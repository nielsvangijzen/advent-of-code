package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strconv"
	"strings"
)

type BagRule struct {
	HoldsMap map[string]int
}

func (rule BagRule) CanHold(name string) bool {
	_, ok := rule.HoldsMap[name]
	return ok
}

func main() {
	input := helpers.MustInputStringArray("2020/day-7/input.txt")
	first(input)
}

func first(input []string) {
	ruleMap := map[string]BagRule{}

	for _, rule := range input {
		name, bagRule := parseRule(rule)
		ruleMap[name] = bagRule
	}
	fmt.Println(ruleMap)
	fmt.Printf("First: %d bags", canHold("shiny gold", ruleMap))

}

func canHold(name string, rules map[string]BagRule) int {
	amount := 0

	for ruleName, rule := range rules {
		if rule.CanHold(name) {
			fmt.Printf("A %s bag can hold a %s bag\n", ruleName, name)
			amount += 1

			if canHold(ruleName, rules) != 0 {
				amount += 1
			}
		}
	}

	return amount
}

func parseRule(rule string) (name string, bagRule BagRule) {
	bagRule = BagRule{
		HoldsMap: map[string]int{},
	}

	split := strings.Split(rule, " contain ")

	// Bag Name
	name = strings.Replace(split[0], " bags", "", -1)

	// Holds
	holdings := strings.Split(split[1], ", ")
	for _, holds := range holdings {
		holds = strings.Replace(holds, ".", "", -1)
		if holds == "no other bags" {
			return
		}

		amount, err := strconv.Atoi(string(holds[0]))
		if err != nil {
			fmt.Printf("Unable to parse %s", holds)
			continue
		}

		name := strings.Replace(holds[2:], " bags", "", -1)
		name = strings.Replace(name, " bag", "", -1)
		bagRule.HoldsMap[name] = amount
	}

	return
}
