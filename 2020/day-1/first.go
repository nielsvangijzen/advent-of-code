package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strconv"
)

func main() {
	nums := parseInput()
	findTwo(nums)
	findThree(nums)
}

func parseInput() (numbers []int64) {
	input := helpers.MustInputStringArray("2020/day-1/input.txt")

	for _, num := range input {
		parsed, err := strconv.ParseInt(num, 10, 64)
		if err != nil {
			fmt.Printf("unable to parse line %s\n", num)
			continue
		}

		numbers = append(numbers, parsed)
	}

	return
}

func findTwo(numbers []int64) {
	for _, intA := range numbers {
		for _, intB := range numbers {
			if intA+intB == 2020 {
				fmt.Printf("%d + %d == 2020\n", intA, intB)
				fmt.Printf("%d * %d == %d\n", intA, intB, intA*intB)
				return
			}
		}
	}
	fmt.Printf("findTwo found nothing\n")
}

func findThree(numbers []int64) {
	for _, intA := range numbers {
		for _, intB := range numbers {
			for _, intC := range numbers {
				if intA+intB+intC == 2020 {
					fmt.Printf("%d + %d + %d == 2020\n", intA, intB, intC)
					fmt.Printf("%d * %d * %d == %d\n", intA, intB, intC, intA*intB*intC)
					return
				}
			}
		}
	}

	fmt.Printf("findThree found nothing\n")
}
