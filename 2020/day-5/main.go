package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"math"
)

type Seat struct {
	Identifier string
	Column     int
	Row        int
}

func (s Seat) SeatID() int {
	return (s.Row * 8) + s.Column
}

func (s Seat) String() string {
	return fmt.Sprintf("%s: row %d, column %d, seat ID %d", s.Identifier, s.Row, s.Column, s.SeatID())
}

func main() {
	inputs := helpers.MustInputStringArray("2020/day-5/input.txt")
	First(inputs)
	Second(inputs)
}

func First(inputs []string) {
	highestID := 0
	for _, input := range inputs {
		seat := findSeat(input)
		id := seat.SeatID()
		if id > highestID {
			highestID = id
		}
	}

	fmt.Printf("First: highest ID: %d\n", highestID)
}

func Second(inputs []string) {
	allIDS := make([]int, len(inputs))
	for index, input := range inputs {
		allIDS[index] = findSeat(input).SeatID()
	}
L:
	for _, idA := range allIDS {
		for _, idB := range allIDS {
			absolute := math.Abs(float64(idB - idA))
			middle := (idA + idB) / 2
			if absolute == 2 && !helpers.IntInSlice(middle, allIDS) {
				fmt.Printf("Second: Found a seat between %d and %d the middle one isn't in the list: %d\n",
					idA, idB, middle)
				break L
			}
		}
	}
}

func makeFilledList(length int) []int {
	arr := make([]int, length)
	for i := 0; i < length; i++ {
		arr[i] = i
	}

	return arr
}

func findSeat(input string) Seat {
	rowArr := makeFilledList(128)
	columnArr := makeFilledList(8)

	for _, chr := range input {
		switch chr {
		case 'F':
			rowArr = rowArr[:len(rowArr)/2]
		case 'B':
			rowArr = rowArr[len(rowArr)/2:]
		case 'L':
			columnArr = columnArr[:len(columnArr)/2]
		case 'R':
			columnArr = columnArr[len(columnArr)/2:]
		}
	}

	return Seat{
		input, columnArr[0], rowArr[0],
	}
}
