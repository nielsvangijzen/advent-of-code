# Day 5

# First
This one was a nice puzzle, I've dabbled around with a few concepts! One of them being min and
max variables which I could then halve down or halve up depending on the letter.

I settled for just creating an array of `length` like so:
```go
arr := make([]int, length)
for i := 0; i < length; i++ {
    arr[i] = i
}
```

I ended up slicing this one down for both the Row and the Number

# Second
This was a little easier, I used a nested for-loop to iterate over all the numbers to find 2 IDs
that had an exact difference of 2. Then I checked if the number in the middle of those 2 numbers
existed in the list. If it didn't then it meant that that was my seat!