package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strconv"
)

func main() {
	input := helpers.MustInputStringArray("2019/day-1/input.txt")
	f := calculateFuel(input, calculate)
	fmt.Printf("First: %d\n", f)

	s := calculateFuel(input, calculate2)
	fmt.Printf("Second: %d\n", s)
}

func calculateFuel(input []string, fuelFunc func(int) int) (total int) {
	for _, value := range input {
		i, err := strconv.Atoi(value)
		if err != nil {
			fmt.Printf("Unable to parse value %s: %s\n", value, err)
			continue
		}
		total += fuelFunc(i)
	}

	return
}

func calculate(weight int) int {
	return (weight / 3) - 2
}

func calculate2(weight int) int {
	fuel := calculate(weight)

	if fuel > 0 {
		fuel += calculate2(fuel)
	}

	if fuel <= 0 {
		return 0
	}

	return fuel
}
