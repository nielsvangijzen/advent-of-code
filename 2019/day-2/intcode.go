package main

import (
	"fmt"
	"reflect"
	"runtime"
)

type AOCProcessor struct {
	rom    []int
	memory []int
	traces []trace

	ip int

	run    bool
	halted bool
}

type trace struct {
	ProcessorState AOCProcessor
	EventType      string
	Message        string
}

var instructions = map[int]func(*AOCProcessor){
	1:  add,
	2:  mult,
	99: halt,
}

func NewAOCProcessor(code []int) *AOCProcessor {
	return &AOCProcessor{
		rom:    code,
		memory: code,
		run:    false,
		halted: false,
	}
}

func (p *AOCProcessor) Dump() []int {
	return p.memory
}

func (p *AOCProcessor) Traces() (output string) {
	for index, trace := range p.traces {
		output += fmt.Sprintf("#%d - %s: %s\n", index, trace.EventType, trace.Message)
	}
	return
}

func (p *AOCProcessor) AddTrace(eventType string, message string) {
	aocCopy := *p
	p.traces = append(p.traces, trace{
		ProcessorState: aocCopy,
		EventType:      eventType,
		Message:        message,
	})
}

func (p *AOCProcessor) Start() {
	p.run = true

	for p.run && !p.halted {
		p.cycle()
	}
}

func (p *AOCProcessor) get(pos int) (value int) {
	if pos >= len(p.memory) || pos < 0 {
		p.except(fmt.Sprintf("trying to access invalid memory address (%d)", pos))
		return
	}

	p.AddTrace("MEMORY_READ", fmt.Sprintf("Reading value at pos %d (%d)", pos, p.memory[pos]))
	return p.memory[pos]
}

func (p *AOCProcessor) set(pos int, value int) {
	if pos >= len(p.memory) || pos < 0 {
		p.except(fmt.Sprintf("trying to access invalid memory address (%d)", pos))
		return
	}

	p.AddTrace("MEMORY_WRITE", fmt.Sprintf("Storing value %d at pos %d", value, pos))
	p.memory[pos] = value
}

func (p *AOCProcessor) getRel(pos int) (value int) {
	return p.get(pos + p.ip)
}

func (p *AOCProcessor) setRel(pos int, value int) {
	p.set(p.ip+pos, value)
}

func (p *AOCProcessor) except(err string) {
	p.AddTrace("EXCEPTION", err)
	p.run = false
}

func (p *AOCProcessor) cycle() {
	// Fetch
	opcode := p.get(p.ip)

	//Decode
	fn, registered := instructions[opcode]
	if !registered {
		p.except(fmt.Sprintf("trying to decode an instruction that's not implemented: %d", opcode))
		return
	}

	insName := runtime.FuncForPC(reflect.ValueOf(fn).Pointer()).Name()
	p.AddTrace("DECODE", fmt.Sprintf("Decoded opcode (%d) as instruction (%s)", opcode, insName))

	// Execute
	fn(p)

	return
}

func halt(p *AOCProcessor) {
	p.halted = true
	p.run = false
}

func add(p *AOCProcessor) {
	// Args: 2, Jumps 4
	left := p.get(p.getRel(1))
	right := p.get(p.getRel(2))
	address := p.getRel(3)

	p.AddTrace("INSTRUCTION", fmt.Sprintf("Adding %d and %d", left, right))

	p.set(address, left+right)
	p.ip += 4
}

func mult(p *AOCProcessor) {
	// Args: 2, Jumps 4
	left := p.get(p.getRel(1))
	right := p.get(p.getRel(2))
	address := p.getRel(3)

	p.AddTrace("INSTRUCTION", fmt.Sprintf("Multiplying %d and %d", left, right))

	p.set(address, left*right)
	p.ip += 4
}
