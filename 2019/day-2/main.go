package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
)

func main() {
	first()
	second()
}

func first() {
	input := helpers.MustInputCsList("2019/day-2/input.txt")
	input[1] = 12
	input[2] = 2
	processor := NewAOCProcessor(input)
	processor.Start()
	fmt.Printf("First: %d\n", processor.get(0))
}

func second() {
	input := helpers.MustInputCsList("2019/day-2/input.txt")

	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			tmp := make([]int, len(input))
			copy(tmp, input)

			tmp[1] = noun
			tmp[2] = verb
			processor := NewAOCProcessor(tmp)
			processor.Start()

			if processor.get(0) == 19690720 {
				fmt.Printf("Second: noun=%d verb=%d 100*%d+%d=%d\n", noun, verb, noun, verb, 100*noun+verb)
				return
			}
		}
	}
}
