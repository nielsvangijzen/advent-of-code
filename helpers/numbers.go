package helpers

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func IntInSlice(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func MustInputIntArray(path string) []int {
	return MustStringToIntSlice(MustInputStringArray(path))
}

func MustInputCsList(path string) []int {
	input := MustInputString(path)
	split := strings.Split(input, ",")
	numbers := make([]int, len(split))
	for index, str := range split {
		numbers[index] = MustAtoI(str)
	}

	return numbers
}

func MustStringToIntSlice(input []string) []int {
	rArr := make([]int, len(input))

	for index, num := range input {
		num, err := strconv.Atoi(num)
		if err != nil {
			fmt.Printf("Unable to parse integers: %s\n", err)
			os.Exit(2)
		}

		rArr[index] = num
	}

	return rArr
}

func MustAtoI(num string) int {
	intNum, err := strconv.Atoi(num)
	if err != nil {
		fmt.Printf("MustAtoI error: %s\n", err)
		os.Exit(2)
	}
	return intNum
}

func GetLowestInSet(numbers []int) int {
	lowest := math.MaxInt32
	for _, num := range numbers {
		if num < lowest {
			lowest = num
		}
	}

	return lowest
}

func GetHighestInSet(numbers []int) int {
	highest := math.MinInt32
	for _, num := range numbers {
		if num > highest {
			highest = num
		}
	}

	return highest
}
