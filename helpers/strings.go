package helpers

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func InputString(path string) (string, error) {
	input, err := os.Open(path)
	if err != nil {
		return "", err
	}

	b, err := ioutil.ReadAll(input)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

func InputStringArray(path string) ([]string, error) {
	input, err := InputString(path)
	if err != nil {
		return []string{}, err
	}

	split := strings.Split(input, "\n")
	if split[len(split)-1] == "" {
		split = split[:len(split)-1]
	}

	return split, nil
}

func MustInputString(path string) string {
	input, err := InputString(path)
	if err != nil {
		fmt.Printf("Couldn't parse input: %s", err)
		os.Exit(2)
	}

	return input
}

func MustInputStringArray(path string) []string {
	input, err := InputStringArray(path)
	if err != nil {
		fmt.Printf("Couldn't parse input: %s", err)
		os.Exit(2)
	}

	return input
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
