package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"sort"
)

type BlockType int
type State int

const (
	PAREN = BlockType(iota)
	SQUARE
	CURLY
	HOOK
)

const (
	RUNNING = State(iota)
	COMPLETE
	INCOMPLETE
	CORRUPTED
)

func (block BlockType) String() string {
	switch block {
	case PAREN:
		return "PAREN"
	case SQUARE:
		return "SQUARE"
	case CURLY:
		return "CURLY"
	case HOOK:
		return "HOOK"
	}
	return ""
}

var CharTypeMap = map[string]BlockType{
	"(": PAREN,
	")": PAREN,
	"[": SQUARE,
	"]": SQUARE,
	"{": CURLY,
	"}": CURLY,
	"<": HOOK,
	">": HOOK,
}

type Parser struct {
	line        string
	pos         int
	blockStack  []BlockType
	currenBlock BlockType
	state       State
}

func NewParser(line string) *Parser {
	return &Parser{
		line: line,
	}
}

func (parser *Parser) Parse() {
	for parser.state == RUNNING {
		expr := parser.CurrentToken()

		switch expr {
		case "(", "[", "{", "<":
			parser.Open(expr)
		case ")", "]", "}", ">":
			parser.Close(expr)
		}

		if parser.state == CORRUPTED {
			return
		}

		if parser.pos == len(parser.line)-1 {
			if len(parser.blockStack) != 0 {
				parser.state = INCOMPLETE
			} else {
				parser.state = COMPLETE
			}
		}

		parser.pos++
	}
}

func (parser *Parser) CurrentToken() string {
	return string(parser.line[parser.pos])
}

func (parser *Parser) Open(opener string) {
	blockType := CharTypeMap[opener]
	parser.blockStack = append(parser.blockStack, blockType)
}

func (parser *Parser) Close(closer string) {
	blockType := CharTypeMap[closer]
	currentBlock := parser.blockStack[len(parser.blockStack)-1]
	if currentBlock != blockType {
		fmt.Printf("Expected closing %s got closing %s\n", currentBlock, blockType)
		parser.state = CORRUPTED
		return
	}

	parser.blockStack = parser.blockStack[:len(parser.blockStack)-1]
}

func main() {
	first()
	second()
}

func first() {
	lines := helpers.MustInputStringArray("2021/day-10/input.txt")

	scoreMap := map[string]int{
		")": 0,
		"]": 0,
		"}": 0,
		">": 0,
	}

	for _, line := range lines {
		parser := NewParser(line)
		parser.Parse()
		if parser.state == CORRUPTED {
			scoreMap[parser.CurrentToken()] += 1
		}
	}

	score := (scoreMap[")"] * 3) + (scoreMap["]"] * 57) + (scoreMap["}"] * 1197) + (scoreMap[">"] * 25137)

	fmt.Printf("Challenge1: %d\n", score)
}

func second() {
	lines := helpers.MustInputStringArray("2021/day-10/input.txt")

	var scores []int

	for _, line := range lines {
		parser := NewParser(line)
		parser.Parse()
		if parser.state == INCOMPLETE {
			score := 0
			for pos := len(parser.blockStack) - 1; pos >= 0; pos-- {
				score *= 5
				switch parser.blockStack[pos] {
				case PAREN:
					score += 1
				case SQUARE:
					score += 2
				case CURLY:
					score += 3
				case HOOK:
					score += 4
				}
			}
			scores = append(scores, score)
		}
	}

	sort.Ints(scores)
	fmt.Printf("Challenge2: %d\n", scores[(len(scores)/2)])
}
