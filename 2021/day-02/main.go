package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"os"
	"strconv"
	"strings"
)

type Direction int

const (
	UP = Direction(iota)
	DOWN
	FORWARD
)

type Instruction struct {
	Direction Direction
	Amount    int
}

func main() {
	first()
	second()
}

func first() {
	input := helpers.MustInputStringArray("2021/day-02/input.txt")

	horizontal := 0
	depth := 0

	for _, ins := range input {
		instruction := parseInstruction(ins)

		switch instruction.Direction {
		case FORWARD:
			horizontal += instruction.Amount
		case DOWN:
			depth += instruction.Amount
		case UP:
			depth -= instruction.Amount
		}
	}

	fmt.Printf("%d * %d = %d\n", horizontal, depth, horizontal*depth)
}

func second() {
	input := helpers.MustInputStringArray("2021/day-02/input.txt")

	horizontal := 0
	depth := 0
	aim := 0

	for _, ins := range input {
		instruction := parseInstruction(ins)

		switch instruction.Direction {
		case FORWARD:
			horizontal += instruction.Amount
			depth += aim * instruction.Amount
		case DOWN:
			aim += instruction.Amount
		case UP:
			aim -= instruction.Amount
		}
	}

	fmt.Printf("%d * %d = %d\n", horizontal, depth, horizontal*depth)
}

func parseInstruction(input string) Instruction {
	arr := strings.Split(input, " ")
	var dir Direction
	switch arr[0] {
	case "up":
		dir = UP
	case "down":
		dir = DOWN
	case "forward":
		dir = FORWARD
	}

	num, err := strconv.Atoi(arr[1])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	return Instruction{
		Direction: dir,
		Amount:    num,
	}
}
