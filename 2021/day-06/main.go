package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strings"
	"time"
)

type Timer struct {
	Fishes int
	Timer  int
}

type TimerContainer struct {
	Timers   []*Timer
	DayCount int
}

func (t TimerContainer) String() (output string) {
	for _, timer := range t.Timers {
		output += fmt.Sprintf("[Count: %d, Fishes: %d] ", timer.Timer, timer.Fishes)
	}
	return
}

func (t TimerContainer) Sum() (sum int) {
	for _, timer := range t.Timers {
		sum += timer.Fishes
	}
	return
}

func NewTimerContainer(nums []int) (t TimerContainer) {
	for _, num := range nums {
		t.AddFishToTimer(num, 1)
	}

	return
}

func (t *TimerContainer) AddFishToTimer(timerCount int, amount int) {
	for _, timer := range t.Timers {
		if timer.Timer == timerCount {
			timer.Fishes += amount
			return
		}
	}

	t.Timers = append(t.Timers, &Timer{
		Fishes: amount,
		Timer:  timerCount,
	})
}

func (t *TimerContainer) RunDay() {
	t.DayCount += 1
	var newTimers []*Timer
	amountToSpawn := 0

	for _, timer := range t.Timers {
		timer.Timer -= 1
		if timer.Timer == -1 {
			amountToSpawn += timer.Fishes
		} else {
			newTimers = append(newTimers, timer)
		}
	}

	newTimers = append(newTimers, &Timer{Fishes: amountToSpawn, Timer: 6})
	newTimers = append(newTimers, &Timer{Fishes: amountToSpawn, Timer: 8})

	t.Timers = newTimers
}

func (t *TimerContainer) SpawnFish(amount int) {
	t.Timers = append(t.Timers, &Timer{
		Fishes: amount,
		Timer:  8,
	})
}

func main() {
	start := time.Now()
	first()
	second()
	fmt.Printf("Took: %f seconds\n", time.Since(start).Seconds())
}

func parseNums(path string) []int {
	input := helpers.MustInputString(path)
	split := strings.Split(input, ",")
	numbers := make([]int, len(split))
	for index, str := range split {
		numbers[index] = helpers.MustAtoI(str)
	}

	return numbers
}

func first() {
	nums := parseNums("2021/day-06/input.txt")
	container := NewTimerContainer(nums)

	for i := 0; i < 80; i++ {
		container.RunDay()
	}

	fmt.Printf("Fishes after 80 days: %d\n", container.Sum())
}

func second() {
	nums := parseNums("2021/day-06/input.txt")
	container := NewTimerContainer(nums)

	for i := 0; i < 256; i++ {
		container.RunDay()
	}

	fmt.Printf("Fishes after 256 days: %d\n", container.Sum())
}
