package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"math"
)

func main() {
	first()
	second()
}

func CalculateFlatCost(pointA int, pointB int) int {
	return int(math.Abs(float64(pointA - pointB)))
}

func CalculateExponentialCost(pointA int, pointB int) int {
	difference := CalculateFlatCost(pointA, pointB)

	// We can turn the given set into a formula so that we
	// don't have to run each calculation recursively/iteratively.

	//   t(n) = 1 + 2 + 3 + ... + n -1 + n
	//   t(n) = n + n - 1 + n - 2 + ... + 2 + 1
	// 2 t(n) = (n + 1) + (n + 1) + (n + 1) + ... + (n + 1)
	// 2 t(n) = n (n + 1)
	//   t(n) = (n(n + 1)) / 2

	// This boils down to t(difference) = 1/2·difference·(difference+1)
	return int(.5 * float64(difference*(difference+1)))
}

func MoveToPosition(numbers []int, position int, costFunc func(int, int) int) (fuel int) {
	for _, number := range numbers {
		fuel += costFunc(number, position)
	}
	return
}

func FindLowestFuelCost(inputNums []int, costFunc func(int, int) int) {
	lowest := helpers.GetLowestInSet(inputNums)
	highest := helpers.GetHighestInSet(inputNums)

	lowestFuel := math.MaxInt32
	lowestFuelPosition := lowest

	for i := lowest; i <= highest; i++ {
		if newPos := MoveToPosition(inputNums, i, costFunc); newPos < lowestFuel {
			lowestFuel = newPos
			lowestFuelPosition = i
		}
	}

	fmt.Printf("Lowest fuel count: %d at position: %d\n", lowestFuel, lowestFuelPosition)
}

func first() {
	inputNums := helpers.MustInputCsList("2021/day-07/input.txt")
	FindLowestFuelCost(inputNums, CalculateFlatCost)
}

func second() {
	inputNums := helpers.MustInputCsList("2021/day-07/input.txt")
	FindLowestFuelCost(inputNums, CalculateExponentialCost)
}
