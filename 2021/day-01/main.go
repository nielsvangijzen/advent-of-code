package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
)

func main() {
	first()
	second()
}

func first() {
	inputNums := helpers.MustInputIntArray("2021/day-01/input.txt")

	// Since there is no previous measurement we assign the first
	// element and iterate from the second
	previousMeasure := inputNums[0]
	increased := 0

	fmt.Printf("%d (N/A - no previous measurement)\n", previousMeasure)

	for _, measurement := range inputNums[1:] {
		if measurement > previousMeasure {
			increased++
			fmt.Printf("%d (increased)\n", measurement)
		} else {
			fmt.Printf("%d (decreased)\n", measurement)
		}

		previousMeasure = measurement
	}

	fmt.Printf("There are %d measurements that are larger than the previous\n", increased)
}

func second() {
	inputNums := helpers.MustInputIntArray("2021/day-01/input.txt")

	previousWindow := inputNums[0] + inputNums[1] + inputNums[2]
	increased := 0
	index := 0

	fmt.Printf("%d (N/A - no previous sum)\n", previousWindow)

	for {
		index++
		if index > len(inputNums)-3 {
			break
		}

		currentWindow := inputNums[index] + inputNums[index+1] + inputNums[index+2]

		if currentWindow > previousWindow {
			increased++
			fmt.Printf("%d (increased)\n", currentWindow)
		} else if currentWindow == previousWindow {
			fmt.Printf("%d (no change)\n", currentWindow)
		} else {
			fmt.Printf("%d (decreased)\n", currentWindow)
		}

		previousWindow = currentWindow
	}

	fmt.Printf("There are %d sums that are larger than the previous\n", increased)
}
