package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"os"
	"sort"
	"strconv"
)

type Grid struct {
	grid [][]int
}

type Coordinate struct {
	x int
	y int
}

func NewGrid(input []string) (grid Grid) {
	grid.grid = make([][]int, len(input))

	for index, row := range input {
		grid.grid[index] = make([]int, len(row))
		for charIndex, char := range row {
			num, err := strconv.Atoi(string(char))
			if err != nil {
				fmt.Println(err)
				os.Exit(2)
			}

			grid.grid[index][charIndex] = num
		}
	}

	return grid
}

func (grid Grid) getPos(x int, y int) (int, bool) {
	if y < 0 || y >= len(grid.grid) {
		return 0, false
	}

	if x < 0 || x >= len(grid.grid[y]) {
		return 0, false
	}

	return grid.grid[y][x], true
}

func (grid Grid) isLowestForPos(x int, y int) (int, bool) {
	centerNum, _ := grid.getPos(x, y)

	for _, relY := range []int{-1, 1} {
		num, available := grid.getPos(x, y+relY)
		if available {
			if num <= centerNum {
				return centerNum, false
			}
		}
	}

	for _, relX := range []int{-1, 1} {
		num, available := grid.getPos(x+relX, y)
		if available {
			if num <= centerNum {
				return centerNum, false
			}
		}
	}

	return centerNum, true
}

func (grid Grid) findBasin(x int, y int, visited []Coordinate) []Coordinate {
	//fmt.Printf("X:%d Y:%d\n", x, y)
	current, available := grid.getPos(x, y)
	if !available || current == 9 {
		return visited
	}

	visited = append(visited, Coordinate{x: x, y: y})

	for _, relX := range []int{-1, 1} {
		if Visited(x+relX, y, visited) {
			continue
		}

		_, available := grid.getPos(x+relX, y)
		if !available {
			continue
		}

		visited = grid.findBasin(x+relX, y, visited)
	}

	for _, relY := range []int{-1, 1} {
		if Visited(x, y+relY, visited) {
			continue
		}

		_, available := grid.getPos(x, y+relY)
		if !available {
			continue
		}

		visited = grid.findBasin(x, y+relY, visited)
	}

	return visited
}

func (grid Grid) PrintCoordinates(coors []Coordinate) {
	line := ""
	for y := 0; y < len(grid.grid); y++ {
		for x := 0; x < len(grid.grid[0]); x++ {
			if Visited(x, y, coors) {
				num, _ := grid.getPos(x, y)
				line += fmt.Sprintf("%d", num)
			} else {
				line += "."
			}
		}
		line += "\n"
	}
	fmt.Printf(line)
}

func Visited(x int, y int, visited []Coordinate) bool {
	for _, coordinate := range visited {
		if x == coordinate.x && y == coordinate.y {
			return true
		}
	}
	return false
}

func main() {
	first()
	second()
}

func first() {
	inputNums := helpers.MustInputStringArray("2021/day-09/input.txt")

	riskSum := 0

	grid := NewGrid(inputNums)
	for x := 0; x < len(grid.grid[0]); x++ {
		for y := 0; y < len(grid.grid); y++ {
			if num, isLowest := grid.isLowestForPos(x, y); isLowest {
				riskSum += num + 1
			}
		}
	}

	fmt.Printf("Challenge 1: %d\n", riskSum)
}

func second() {
	inputNums := helpers.MustInputStringArray("2021/day-09/input.txt")
	grid := NewGrid(inputNums)
	var basins []int

	for x := 0; x < len(grid.grid[0]); x++ {
		for y := 0; y < len(grid.grid); y++ {
			if _, isLowest := grid.isLowestForPos(x, y); isLowest {
				coors := grid.findBasin(x, y, []Coordinate{})
				basins = append(basins, len(coors))
				//grid.PrintCoordinates(coors)
				//fmt.Println()
			}
		}
	}

	product := 1
	sort.Ints(basins)
	for _, size := range basins[len(basins)-3:] {
		product *= size
	}

	fmt.Printf("Challenge 2: %d\n", product)
}
