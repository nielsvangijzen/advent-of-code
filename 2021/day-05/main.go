package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"strings"
)

type Grid struct {
	grid [][]int
}

type Line struct {
	x1, x2, y1, y2 int
}

func NewGrid(width int, height int) Grid {
	grid := make([][]int, height+1)
	for index := range grid {
		grid[index] = make([]int, width+1)
	}

	return Grid{
		grid: grid,
	}
}

func (g Grid) String() (output string) {
	for index, row := range g.grid {
		output += fmt.Sprintf("%d: ", index)
		for _, column := range row {
			if column == 0 {
				output += "."
			} else {
				output += fmt.Sprintf("%d", column)
			}
		}
		output += "\n"
	}
	return
}

func (g Grid) MarkLine(line Line) {
	xSign := -1
	ySign := -1
	if line.x1 < line.x2 {
		xSign = 1
	} else if line.x1 == line.x2 {
		xSign = 0
	}
	if line.y1 < line.y2 {
		ySign = 1
	} else if line.y1 == line.y2 {
		ySign = 0
	}

	x := line.x1
	y := line.y1
	running := true
	for running {
		if x == line.x2 && y == line.y2 {
			running = false
		}
		g.grid[y][x] += 1
		x += xSign
		y += ySign
	}
}

func (g Grid) MarkStraight(line Line) {
	if line.x1 != line.x2 && line.y1 != line.y2 {
		return
	}

	g.MarkLine(line)
}

func (g Grid) LargerThanTwo() (sum int) {
	for _, row := range g.grid {
		for _, column := range row {
			if column > 1 {
				sum += 1
			}
		}
	}
	return
}

func main() {
	first()
	second()
}

func ParseInput(input []string) (lines []Line, maxX int, maxY int) {
	for _, line := range input {
		parts := strings.Split(line, " -> ")
		pointA := strings.Split(parts[0], ",")
		pointB := strings.Split(parts[1], ",")

		x1 := helpers.MustAtoI(pointA[0])
		y1 := helpers.MustAtoI(pointA[1])
		x2 := helpers.MustAtoI(pointB[0])
		y2 := helpers.MustAtoI(pointB[1])

		if x1 > maxX {
			maxX = x1
		}
		if x2 > maxX {
			maxX = x2
		}
		if y1 > maxY {
			maxY = y1
		}
		if y2 > maxY {
			maxY = y2
		}

		lines = append(lines, Line{x1: x1, x2: x2, y1: y1, y2: y2})
	}
	return
}

func first() {
	input := helpers.MustInputStringArray("2021/day-05/input.txt")
	lines, maxX, maxY := ParseInput(input)
	grid := NewGrid(maxX, maxY)

	for _, line := range lines {
		grid.MarkStraight(line)
	}

	fmt.Println(grid.LargerThanTwo())
}

func second() {
	input := helpers.MustInputStringArray("2021/day-05/input.txt")
	lines, maxX, maxY := ParseInput(input)
	grid := NewGrid(maxX, maxY)

	for _, line := range lines {
		grid.MarkLine(line)
	}

	fmt.Println(grid.LargerThanTwo())
}
