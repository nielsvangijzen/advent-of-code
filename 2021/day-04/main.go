package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"os"
	"strconv"
	"strings"
)

type BingoBoard struct {
	board [5][5]*BingoCell
}

type BingoCell struct {
	num    int
	marked bool
}

func (cell BingoCell) String() string {
	return fmt.Sprintf("[%d]|%v", cell.num, cell.marked)
}

func newBingoBoard(input string) BingoBoard {
	board := BingoBoard{
		board: [5][5]*BingoCell{},
	}

	lines := strings.Split(input, "\n")

	for row, line := range lines {
		columns := strings.Fields(line)
		for column, stringNum := range columns {
			num, err := strconv.Atoi(stringNum)
			if err != nil {
				fmt.Printf("Newboard: %s", err)
				os.Exit(2)
			}
			board.board[row][column] = &BingoCell{
				num:    num,
				marked: false,
			}
		}
	}

	return board
}

func (b *BingoBoard) Mark(num int) {
	for _, row := range b.board {
		for _, cell := range row {
			if cell.num == num {
				cell.marked = true
			}
		}
	}
}

func (b *BingoBoard) String() string {
	return fmt.Sprintf("%s", b.board)
}

func (b BingoBoard) DidWin() bool {
	// First we check horizontal lines
	for _, row := range b.board {
		foundUnmarked := false

		for _, cell := range row {
			if !cell.marked {
				foundUnmarked = true
				break
			}
		}

		if !foundUnmarked {
			return true
		}
	}

	for column := 0; column < 5; column++ {
		foundUnmarked := false
		for row := 0; row < 5; row++ {
			if !b.board[row][column].marked {
				foundUnmarked = true
				break
			}
		}
		if !foundUnmarked {
			return true
		}
	}

	return false
}

func (b BingoBoard) CalculateScore(num int) int {
	sum := 0
	for _, row := range b.board {
		for _, cell := range row {
			if !cell.marked {
				sum += cell.num
			}
		}
	}

	fmt.Printf("Sum: %d; Num: %d;\n", sum, num)
	return sum * num
}

func main() {
	first()
	second()
}

func first() {
	input := helpers.MustInputString("2021/day-04/input.txt")
	parts := strings.Split(input, "\n\n")
	pickOrder := helpers.MustStringToIntSlice(strings.Split(parts[0], ","))

	var boards []BingoBoard

	for _, board := range parts[1:] {
		boards = append(boards, newBingoBoard(board))
	}

	for numIndex, num := range pickOrder {
		for index, board := range boards {
			board.Mark(num)
			if board.DidWin() {
				fmt.Printf("Board [%d] won after drawing %v\n", index+1, pickOrder[:numIndex+1])
				answer := board.CalculateScore(num)
				fmt.Printf("Answer: %d\n", answer)
				return
			}
		}
	}
}

func second() {
	input := helpers.MustInputString("2021/day-04/input.txt")
	parts := strings.Split(input, "\n\n")
	pickOrder := helpers.MustStringToIntSlice(strings.Split(parts[0], ","))

	var boards []BingoBoard

	for _, board := range parts[1:] {
		boards = append(boards, newBingoBoard(board))
	}

	var boardsThatWon []int

	for numIndex, num := range pickOrder {
		for index, board := range boards {
			if helpers.IntInSlice(index, boardsThatWon) {
				continue
			}

			board.Mark(num)
			if board.DidWin() {
				fmt.Printf("Board [%d] won after drawing %v\n", index+1, pickOrder[:numIndex+1])
				boardsThatWon = append(boardsThatWon, index)
			}

			if len(boardsThatWon) == len(boards) {
				fmt.Printf("Last board won: board %d\n", index+1)
				fmt.Printf("Answer: %d\n", board.CalculateScore(num))
			}
		}
	}
}
