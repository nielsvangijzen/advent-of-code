package main

import (
	"fmt"
	"gitlab.com/nielsvangijzen/adventofcode/helpers"
	"os"
	"strconv"
)

func main() {
	first()
	second()
}

func first() {
	inputStrings := helpers.MustInputStringArray("2021/day-03/input.txt")
	bitLen := len(inputStrings[0])

	gammaRate := ""
	epsilonRate := ""

	for i := 0; i < bitLen; i++ {
		zeroes := 0
		ones := 0

		for _, binary := range inputStrings {
			bit := binary[i]
			if bit == '0' {
				zeroes++
			} else {
				ones++
			}
		}

		if zeroes > ones {
			gammaRate += "0"
			epsilonRate += "1"
		} else {
			gammaRate += "1"
			epsilonRate += "0"
		}
	}

	gamma, err := strconv.ParseInt(gammaRate, 2, 64)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(2)
	}

	epsilon, err := strconv.ParseInt(epsilonRate, 2, 64)
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(2)
	}

	fmt.Printf("1: %d * %d = %d\n", gamma, epsilon, gamma*epsilon)
}

func second() {
	inputStrings := helpers.MustInputStringArray("2021/day-03/input.txt")

	oxygen := findCommonInList(inputStrings, true)
	co2 := findCommonInList(inputStrings, false)

	fmt.Printf("1: %d * %d = %d\n", oxygen, co2, oxygen*co2)
}

func findCommonInList(numbers []string, most bool) int64 {
	bitLen := len(numbers[0])

	for i := 0; i < bitLen; i++ {
		var zeroes []string
		var ones []string

		for _, binary := range numbers {
			bit := binary[i]
			if bit == '0' {
				zeroes = append(zeroes, binary)
			} else {
				ones = append(ones, binary)
			}
		}

		if len(zeroes) == len(ones) {
			if most {
				numbers = ones
			} else {
				numbers = zeroes
			}
		}

		zeroesBigger := len(zeroes) > len(ones)

		if (most && zeroesBigger) || (!most && !zeroesBigger) {
			numbers = zeroes
		} else {
			numbers = ones
		}

		if len(numbers) == 1 {
			gamma, err := strconv.ParseInt(numbers[0], 2, 64)
			if err != nil {
				fmt.Printf("Error: %s\n", err)
				os.Exit(2)
			}
			return gamma
		}
	}

	return -1
}
